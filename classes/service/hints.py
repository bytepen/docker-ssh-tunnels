HINTS = {
    "bytepen/ssh_tunnels:start": [
        "- Once properly configured, check your configuration by running the command 'check' on the start machine."
    ],
    "bytepen/ssh_tunnels:target": [
        "- The target has a web server on port 80.",
        "- Make it so you can view this web server through a local port on this machine."
    ],
    "bytepen/ssh_tunnels:target_beacon": [
        "- The target is beaconing to the box you're on right before it.",
        "- The beacon will give you a shell",
        "- Make sure you can catch the beacon on your start box (ex: 'nc 127.0.0.1 12345')."
    ]
}
