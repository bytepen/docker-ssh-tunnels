#!/bin/bash

for d in */ ; do
	docker build ./${d%/} -t bamhm182/ssh_tunnels:${d%/}
	docker push bamhm182/ssh_tunnels:${d%/}
done
